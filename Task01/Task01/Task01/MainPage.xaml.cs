﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Task01
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        void OnButton1Click(object sender, EventArgs e)
        {
            var a = SumOfMultiples.FindSum(InputCollection.Text);
            Answer.Text = ("The Total Sum is: " + a);
        }
    }
}
