﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task01
{
    class SumOfMultiples
    {
        public static int FindSum(string UserInput)
        {
            int i;
            int Total = 0;
            int UserNumber = int.Parse(UserInput);

            for (i = 1; i < UserNumber; i++)
            {
                if ((i % 3 == 0) || (i % 5 == 0))
                {
                    Total = Total + i;
                }
            }
            return Total;
        }
    }
}
