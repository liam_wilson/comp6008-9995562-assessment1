﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Task02
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        void OnButton1Click(object sender, EventArgs e)
        {
            var UserNumber = int.Parse(InputCollection.Text);
            var a = new CreateAnswer(UserNumber);
            var RandNum = a.CreateRand();
            var ChangeColor = a.DetermineColour(RandNum);

            Background.BackgroundColor = Color.FromHex($"{ChangeColor}");
        }
    }
}
