﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task02
{
    class CreateAnswer
    {
        public int CompNumber;
        public CreateAnswer(int _number)
        {
            CompNumber = _number;
        }

        public int CreateRand()
        {
            Random Rand = new Random();
            int Answer = Rand.Next(1, 11);
            return Answer;
        }

        public string DetermineColour(int UserNumber)
        {
            string ChangeColour;

            if (CompNumber == UserNumber)
            {
                ChangeColour = "#00ff00";
            }
            else
            {
                ChangeColour = "#ff0000";
            }

            return ChangeColour;
        }
    }
}
